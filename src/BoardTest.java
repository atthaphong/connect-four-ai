import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

public class BoardTest {
	int rows;
	int cols;
	@Before
	public void setUp() throws Exception {
		rows = 6;
		cols = 7;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateBoardWithString() throws Exception{
		String configString = "BWBOOOWBWBWOBWBWBOWBWBWOBWBWBOWBWBWOBWBOOO";
		Board a = new Board(rows,cols,configString,Board.BLACK,Board.BLACK);
		
		int[][] configArray = {
				{Board.BLACK,Board.WHITE,Board.BLACK,Board.WHITE,Board.BLACK,Board.WHITE,Board.BLACK},
				{Board.WHITE,Board.BLACK,Board.WHITE,Board.BLACK,Board.WHITE,Board.BLACK,Board.WHITE},
				{Board.BLACK,Board.WHITE,Board.BLACK,Board.WHITE,Board.BLACK,Board.WHITE,Board.BLACK},
				{Board.HOLE,Board.BLACK,Board.WHITE,Board.BLACK,Board.WHITE,Board.BLACK,Board.HOLE},
				{Board.HOLE,Board.WHITE,Board.BLACK,Board.WHITE,Board.BLACK,Board.WHITE,Board.HOLE},
				{Board.HOLE,Board.HOLE,Board.HOLE,Board.HOLE,Board.HOLE,Board.HOLE,Board.HOLE}
		};
		Board b = new Board(rows,cols,configArray,Board.BLACK,Board.BLACK);
		assertEquals(b,b);
	}
	
	@Test
	public void testCheckWinHorizonally() throws Exception{
		String configString = "BBBBWWOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO";
		Board a = new Board(rows,cols,configString,Board.BLACK,Board.BLACK);
		assertTrue(a.isCurrentPlayerWon());
		assertFalse(a.tie());	
	}
	
	@Test
	public void testCheckWinVertically() throws Exception{
		String configString = "BBWWBW";
		configString +="BWBWBO";
		configString +="BBBWOO";
		configString +="BWWBBB";
		configString +="BBWBWB";
		configString +="WWBBWB";
		configString +="BWBWBW";
		Board a = new Board(rows,cols,configString,Board.BLACK,Board.BLACK);
		assertTrue(a.isCurrentPlayerWon());
		assertFalse(a.tie());	
	}
	@Test
	public void testCheckWinDiagonally() throws Exception{
		String configString = "BOOOOO";
		configString +="WBOOOO";
		configString +="WWBOOO";
		configString +="WWBBOO";
		configString +="OOOOOO";
		configString +="OOOOOO";
		configString +="OOOOOO";
		Board a = new Board(rows,cols,configString,Board.BLACK,Board.BLACK);
		assertTrue(a.isCurrentPlayerWon());
		assertFalse(a.tie());	
	}
	@Test
	public void testCheckTie() throws Exception{
		String configString = "BBWWBW";
		configString +="BWBWBW";
		configString +="WBBWWW";
		configString +="BWWBBB";
		configString +="WBWBWB";
		configString +="WWBBWB";
		configString +="BWBWBW";
		Board a = new Board(rows,cols,configString,Board.BLACK,Board.BLACK);
		assertFalse(a.isCurrentPlayerWon());
		assertTrue(a.tie());
	}
	@Test
	public void testBoardinHashTable() throws Exception{
		String configString = "BBWWBW";
		configString +="BWBWBW";
		configString +="WBBWWW";
		configString +="BWWBBB";
		configString +="WBWBWB";
		configString +="WWBBWB";
		configString +="BWBWBW";
		Board a = new Board(rows,cols,configString,Board.BLACK,Board.BLACK);
		Board b = new Board(rows,cols,configString,Board.BLACK,Board.BLACK);
		
		Map<Board,Integer> map = new TreeMap<Board,Integer>();
		
		assertEquals(a,b);
		map.put(a, new Integer(1));
		
		assertTrue(map.containsKey(b));
		b.maxPlayerColor = Board.WHITE;
		assertFalse(map.containsKey(b));

	}
//	@Test
	public void testFillable() throws Exception{
		String configString = "      ";
		configString +="      ";
		configString +="      ";
		configString +="      ";
		configString +="      ";
		configString +="      ";
		configString +="      ";
		Board a = new Board(rows,cols,configString,Board.BLACK,Board.BLACK);
		for(int i=0;i<6;i++){
			Board b = a.fill(0);
			b.show();
			a=b;
		}
		System.out.println(a.fillable().size());
		fail();
	}
}
