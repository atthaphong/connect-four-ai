
public class Matching {

	static String ip = "10.10.9.28";
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		playWhite();
	}
	
	public static void playBlack() {
		// TODO Auto-generated method stub
		Connector bc = new Connector(ip,8002);
		
		Player black = new Player(bc,Board.BLACK,Player.ALPHABETA);
		while(true) { 
			String st = bc.Receive(); // 
			if(st.toLowerCase().indexOf("end game")== 0) {
				break;
			}
			System.out.println("Black Move: Start state is "+st);
			// Add your method here and send the answer to the server
			try {
				black.read(new Board(st,Board.BLACK));
				black.play();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}
		System.out.println("Game ended");
	}
	
	public static void playWhite() {
		// TODO Auto-generated method stub
		Connector wc = new Connector(ip,8001);
		
		Player white = new Player(wc,Board.WHITE,Player.ALPHABETA);
		while(true) { 
			String st = wc.Receive(); // 
			if(st.toLowerCase().indexOf("end game")== 0) {
				break;
			}
			System.out.println("White Move: Start state is "+st);
			// Add your method here and send the answer to the server
			try {
				white.read(new Board(st,Board.WHITE));
				white.play();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}
		System.out.println("Game ended");
	}
}
