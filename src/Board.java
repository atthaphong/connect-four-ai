import java.util.Iterator;
import java.util.ArrayList;

public class Board implements Comparable{
	public static final int HOLE = 0;
	public static final int BLACK = 1;
	public static final int WHITE = 2;
	
	public static final int DEFAULT_ROWS = 6;
	public static final int DEFAULT_COLS = 7;
	
	public static final int MAX_PLAYER = 0;
	public static final int MIN_PLAYER = 1;
	
	int cols;
	int rows;
	int[][] board;
	int[] topmostPosOfCols;
	int currentPlayerColor;
	int maxPlayerColor;
	int minPlayerColor;
	int value;
	public int action;
	
	public Board(int rows,int cols,String config,int maxPlayer,int currentPlayer) throws Exception{
		init(rows,cols,maxPlayer,currentPlayer);
		for(int col=0;col<cols;col++) topmostPosOfCols[col]=0;
		this.configBoard(config);
	}
	
	public Board(int rows,int cols,int[][] config,int maxPlayer,int currentPlayer) throws Exception{
		init(rows,cols,maxPlayer,currentPlayer);
		for(int col=0;col<cols;col++) topmostPosOfCols[col]=0;
		this.configBoard(config);
	}
	
	private void init(int rows,int cols,int maxPlayer,int currentPlayer){
		this.rows = rows;
		this.cols = cols;
		this.action = -1;
		this.maxPlayerColor = maxPlayer;
		
		if(this.maxPlayerColor==Board.BLACK) this.minPlayerColor = Board.WHITE;
		else this.minPlayerColor = Board.BLACK;
		
		this.currentPlayerColor = currentPlayer;
		this.board = new int[rows][cols];
		this.topmostPosOfCols = new int[cols];
	}
	
	public Board(String config,int currentPlayer) throws Exception{
		this(Board.DEFAULT_ROWS,Board.DEFAULT_COLS,config,currentPlayer,currentPlayer);
	}
	
	public Board(int[][] config,int currentPlayer) throws Exception{
		this(Board.DEFAULT_ROWS,Board.DEFAULT_COLS,config,currentPlayer,currentPlayer);
	}
	
	public Board(String config) throws Exception{
		this(Board.DEFAULT_ROWS,Board.DEFAULT_COLS,config,Board.BLACK,Board.BLACK);
	}
	
	public Board(int[][] config) throws Exception{
		this(Board.DEFAULT_ROWS,Board.DEFAULT_COLS,config,Board.BLACK,Board.BLACK);
	}

	public boolean isCurrentPlayerWon(){
		return won(this.currentPlayerColor);
	}
	
	public boolean isOppositePlayerWon(){
		if(currentPlayerColor==Board.BLACK) return won(Board.WHITE);
		else return won(Board.BLACK);
	}
	
	public boolean won(int color){
		boolean win = false;
		
		/*
		 *  Check vertically
		 */
		for(int col=0;col<cols;col++){
			for(int row=0;row<=rows-4;row++){
				win = (board[row][col]==color)&&(board[1+row][col]==color)&&(board[2+row][col]==color)&&(board[3+row][col]==color);
				if(win) return true;			
			}
		}
		/*
		 * Check horizontally
		 */
		for(int row=0;row<rows;row++){
			for(int col=0;col<=cols-4;col++){
				win = (board[row][0+col]==color)&&(board[row][1+col]==color)&&(board[row][2+col]==color)&&(board[row][3+col]==color);
				if(win) return true;

			}
		}
		/*
		 * Check diagonally upward
		 */
		for(int col=0;col<=cols-4;col++){
			for(int row=0;row<=rows-4;row++){
				win = (board[0+row][0+col]==color)&&(board[1+row][1+col]==color)&&(board[2+row][2+col]==color)&&(board[3+row][3+col]==color);
				if(win) return true;
			}
		}
		/*
		 * Check diagonally downward
		 */
		for(int col=0;col<=cols-4;col++){
			for(int row=rows-1;row>rows-4;row--){
				win = (board[row][0+col]==color)&&(board[row-1][1+col]==color)&&(board[row-2][2+col]==color)&&(board[row-3][3+col]==color);
				if(win) return true;
			}
		}
		return false;
	}

	public boolean tie(){
		for(int row=0;row<rows;row++){
			for(int col=0;col<cols;col++){
				if(board[row][col]==Board.HOLE){
					return false;
				}
			}
		}
		return true;
	}
	
	public void configBoard(int[][] config) throws Exception{
		if(config.length!=rows || config[0].length!=cols){
			String error = "Wrong board configuration.";
			error += config.length+","+rows;
			error += "|";
			error += config[0].length+","+cols;
			throw new Exception(error);
		}
		for(int col=0;col<cols;col++){
			for(int row=0;row<rows;row++){
				board[row][col] = config[row][col];
				if(board[row][col]!=Board.HOLE){
					this.topmostPosOfCols[col]=row;
				}
			}
		}
	}
	public void configBoard(String config) throws Exception{
		if(config.length()!=rows*cols){
			String error = "Wrong board configuration. ";
			error += config.length()+","+rows*cols;
			error += ".";
			throw new Exception(error);
		}
		/*
		 * Convert one dimensional string to board configuration
		 * Board state is sent as column-wise string from down-to-top and left-to-right  
		 * O = Hole
		 * W = White
		 * B = Black
		 * ex. for 3x3 board with configuration below
		 * OOW
		 * OWW
		 * BBB
		 * will be sent as "BOOBWOBWW"
		 */
		
		for(int col=0;col<cols;col++){
			for(int row=0;row<rows;row++){
				int i = col*rows+row;
				char c = config.charAt(i);
				if(c=='B'){
					this.board[row][col] = Board.BLACK;
				}else if(c=='W'){
					this.board[row][col] = Board.WHITE;
				}else{
					this.board[row][col] = Board.HOLE;
				}
				
				if(c=='B' || c=='W'){
					this.topmostPosOfCols[col]=row;
				}
			}
		}
	}
	public boolean equals(Object object){
		Board other = (Board) object;
		if(this.rows!=other.rows || this.cols!=other.cols) return false;
		
		for(int row=0;row<rows;row++){
			for(int col=0;col<cols;col++){
				if(this.board[row][col]!=other.board[row][col]) return false;
			}
		}
		if(this.maxPlayerColor!=other.maxPlayerColor){
			return false;
		}
		
		return true;
	}
	
	public void show(){
		System.out.println("### Board ###");
		for(int row=rows-1;row>=0;row--){
			for(int col=0;col<cols;col++){
				if(board[row][col]==BLACK){
					System.out.print("B");
				}else if(board[row][col]==WHITE){
					System.out.print("W");
				}else if(board[row][col]==HOLE){
					System.out.print("O");
				}else{
					System.out.print("E");
				}
			}
			System.out.println();
		}
		System.out.println("### Bottom ###");
	}
	
	public Board clone(){
		Board clone = null;
		try {
			clone = new Board(rows,cols,this.board,maxPlayerColor,currentPlayerColor);
			clone.topmostPosOfCols = this.topmostPosOfCols.clone();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return clone;
	}
	
	public Board fill(int col){
		Board newBoard = this.clone();
		int row = newBoard.topmostPosOfCols[col];
		newBoard.action = col;
		newBoard.board[row][col] = newBoard.currentPlayerColor;
		newBoard.topmostPosOfCols[col] = row+1;
		newBoard.changeTurn();
		return newBoard;
	}
	
	public boolean isMaxState(){
		return this.currentPlayerColor==this.maxPlayerColor;
	}
	
	public boolean isMinState(){
		return !isMaxState();
	}
	
	public void changeTurn(){
		if(this.currentPlayerColor==Board.BLACK){
			this.currentPlayerColor=Board.WHITE;
		}else{
			this.currentPlayerColor=Board.BLACK;
		}
	}
	
	public ArrayList<Integer> fillable(){
		ArrayList<Integer> fillable = new ArrayList<Integer>();
		for(int col=0;col<cols;col++){
			if(this.topmostPosOfCols[col]<this.rows-2){
				fillable.add(col);
			}
		}
		int i = fillable.indexOf(new Integer(4));
		if(i>=0){
			Integer middle = fillable.get(i);
			fillable.remove(i);
			fillable.add(0, middle);
		}
		return fillable;
	}
	
	

	@Override
	public int compareTo(Object arg0) {
		Board other = (Board) arg0;
		if(this.equals(other)) return 0;
		else if(other.value > this.value) return -1;
		else return 1;
	}

}
