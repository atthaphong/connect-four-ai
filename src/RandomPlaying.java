import java.util.ArrayList;
public class RandomPlaying implements PlayingStrategy {
	
	int color;
	Board board;
	
	public RandomPlaying(int color){
		this.color = color;
	}
	
	@Override
	public void read(Board board) {		
		this.board = board;
	}

	@Override
	public int play() {
		ArrayList<Integer> fillable = board.fillable();
		int rand = (int)(Math.random()*fillable.size());
		return fillable.get(rand)+1;
	}

}
