
public class Player {
	static final int RANDOM = 0;
	static final int HUMAN = 1;
	static final int MINIMAX = 2;
	static final int ALPHABETA = 3;
	
	Connector connector;
	PlayingStrategy strategy;
	int color;
	
	public Player(Connector c,int color,int strategy){
		this.connector = c;
		this.color = color;
			
		if(strategy==RANDOM){
			this.strategy = new RandomPlaying(color);
		}else if(strategy==HUMAN){
			this.strategy = new HumanPlaying(color);
		}else if(strategy==MINIMAX){
			this.strategy = new MinimaxPlaying(color);
		}else if(strategy==ALPHABETA){
			this.strategy = new MinimaxPlaying(color,8);
		}else{
			this.strategy = new RandomPlaying(color);
		}
		
		
	}
	
	public void read(Board board){
		strategy.read(board);
	}
	
	public void play(){
		int col = strategy.play();
		connector.Send(col);
	}
	
}
