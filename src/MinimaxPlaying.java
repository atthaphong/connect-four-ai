import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
public class MinimaxPlaying implements PlayingStrategy {
	public static final long TIME_LIMIT = 4000;
	public static final int DEFAULT_DEPTH = 5;
	public static final int INFINITY = 10000000;
	public static final int WIN_UTILITY = 10000;
	public static final int LOSE_UTILITY = -10000;
	public static final int TIE_UTILITY = 0;
	public static final int MIN_DEPTH = 4;
	public static final int DEFAULT_CUTOFF = 7;
	
	public static final int[][] UTILITY_TABLE = {
		{3,4,5,7,5,4,3},
		{4,6,8,10,8,6,4},
		{5,8,11,13,11,8,5},
		{5,8,11,13,11,8,5},
		{4,6,8,10,8,6,4},
		{3,4,5,7,5,4,3}
	};

	
	public int cutoff;
	public int color;
	
	Board board;
	long startTime;
	Board alphaBoard;
	public  int cutoff_depth = 7;

	public MinimaxPlaying(int color,int depth){
		this.color = color;
		this.cutoff_depth = depth;
	}

	public MinimaxPlaying(int color){
		this(color,DEFAULT_CUTOFF);
	}
	
	@Override
	public void read(Board board) {
		this.board = board;
	}


	
	@Override
	public int play() {
		for(int i=MIN_DEPTH;i<=cutoff_depth;i++){
			if(value(board,i,-INFINITY,INFINITY)==MinimaxPlaying.WIN_UTILITY) break;
		}
		int col = alphaBoard.action + 1;
		System.out.println("Playing col:"+col);
		return col;

	}

	private int value(Board board,int depth,int alpha,int beta){
		int v = -INFINITY;

			
		if(depth<=0){
			v= evalBoard2(board);
		}else if(board.isCurrentPlayerWon()){
			v= MinimaxPlaying.WIN_UTILITY; 
		}else if(board.isOppositePlayerWon()){
			v= MinimaxPlaying.LOSE_UTILITY;
		}else if(board.tie()){
			v= MinimaxPlaying.TIE_UTILITY;
		}else if(board.isMaxState()){
			v= maxValue(board,depth,alpha,beta);
		}else if(board.isMinState()){
			v= minValue(board,depth,alpha,beta);
		}
		board.value = v;
		return v;
	}
	
	private int maxValue(Board board,int depth,int alpha,int beta){
		int max = -MinimaxPlaying.INFINITY;
		ArrayList<Integer> fillable = board.fillable();
		for(int i=0;i<fillable.size();i++){
			int action = fillable.get(i).intValue();
			Board child = board.fill(action);
			int v = value(child,depth-1,alpha,beta);
			v = Math.max(alpha, max);
			if(max>=beta) return v;
			if(alpha<=v){
				alpha = v;
				alphaBoard = child;
			}
		}
		return max;
	}
	
	private int minValue(Board board,int depth,int alpha,int beta){
		int min = MinimaxPlaying.INFINITY;
		ArrayList<Integer> fillable = board.fillable();
		for(int i=0;i<fillable.size();i++){
			int action = fillable.get(i).intValue();
			Board child = board.fill(action);
			int v = value(child,depth-1,alpha,beta);
			min = Math.min(v, min);
			if(min<=alpha) return min;
			beta = Math.min(beta, min);
		}
		return min;
	}

	
	private int evalBoard(Board board){
		int eval = 0;

		
		// row heuristic
		for(int row=0;row<board.rows;row++){
			for(int offset=0;offset<3;offset++){
				int maxCount = 0;
				for(int col=0;col<3;col++){
					if(board.board[row][col+offset]==board.maxPlayerColor) maxCount++;
					else if(board.board[row][col+offset]==board.minPlayerColor) maxCount--;
				}
				eval += utility(maxCount);
			}
		}
		// columns heuristic
		for(int col=0;col<board.cols;col++){
			for(int offset=0;offset<2;offset++){
				int maxCount = 0;
				for(int row=0;row<3;row++){
					if(board.board[row+offset][col]==board.maxPlayerColor) maxCount++;
					else if(board.board[row+offset][col]==board.minPlayerColor) maxCount--;

				}
				eval += utility(maxCount);

			}
		}
		// upward diagonal heuristic
		for(int offsetY=0;offsetY<2;offsetY++){
			for(int row=0;row<3;row++){
				for(int col=0;col<3;col++){
					int maxCount = 0;
					for(int offsetX=0;offsetX<3;offsetX++){
						if(board.board[row+offsetY][col+offsetX]==board.maxPlayerColor) maxCount++;
						else if(board.board[row+offsetY][col+offsetX]==board.minPlayerColor) maxCount--;
					}
					eval += utility(maxCount);
					
				}
			}
		}
		
		// downward diagonal heuristic
	

		return eval;
	}
	
	public int evalBoard2(Board board){
		int eval = 0;
		for(int col=0;col<board.cols;col++){
			for(int row=0;row<board.rows;row++){
				if(board.board[row][col]==board.maxPlayerColor){
					eval += UTILITY_TABLE[row][col];
				}
//				}else if(board.board[row][col]==board.minPlayerColor){
//					eval -= 2*UTILITY_TABLE[row][col];
//				}else{
//					eval += UTILITY_TABLE[row][col];
//				}
			}
		}
		return eval;
	}
	
	public static int utility(int numInLine){
		final int ONE_IN_LINE = 1;
		final int TWO_IN_LINE = 4;
		final int THREE_IN_LINE = 32;
		switch(numInLine){
			case 3: return THREE_IN_LINE;
			case 2: return TWO_IN_LINE;
			case 1: return ONE_IN_LINE;
			case 0: return 0;
			case -1: return ONE_IN_LINE;
			case -2: return TWO_IN_LINE;
			case -3: return 3*THREE_IN_LINE;
			default:
				return 0;
		}
	}
	
}
