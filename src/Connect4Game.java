
public class Connect4Game {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connector wc = new Connector("127.0.0.1",8001);
		Connector bc = new Connector("127.0.0.1",8002);
		
		Player white = new Player(wc,Board.WHITE,Player.RANDOM);
		Player black = new Player(bc,Board.BLACK,Player.ALPHABETA); 
		while(true) { 
			String st = wc.Receive(); // 
			if(st.toLowerCase().indexOf("end game")== 0) {
				break;
			}
			System.out.println("White Move: Start state is "+st);
			// Add your method here and send the answer to the server
			try {
				white.read(new Board(st,Board.WHITE));
				white.play();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			st = bc.Receive(); // 
			if(st.toLowerCase().indexOf("end game")== 0) {
				break;
			}
			System.out.println("Black Move: Start state is "+st);
			try {
				black.read(new Board(st,Board.BLACK));
				black.play();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		System.out.println("Game ended");
	}

}
