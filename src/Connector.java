import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class Connector  {
	private String ip;
	private int port;
	private String status="";
	private String filename;
	private String result;
	private Socket skt;
	public String getStatus() {
		return status;
	}
	public Connector(String inIP,int inPort) {
		ip = inIP;
		port = inPort;
		try {
			skt = new Socket(ip, port);
			showStatus ("Server connected.");
		}
		catch(Exception ex) {
			showStatus(ex.getMessage());
		}
	}
	public void CloseConnection() {
		try {
			skt.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			showStatus(e.getMessage());
		}
	}
	public void setIP(String aIP) {
		ip = aIP;
	}
	public void setPort(int aPort) {
		port = aPort;
	}
	public void showStatus(String in) {
		System.out.println(in);
	}
	public String Receive() {
		byte[] bData = new byte[100];
		try {
			DataInputStream in = new DataInputStream(skt.getInputStream());
			in.read(bData);
			showStatus("Board data received.");
			return new String(bData,"UTF8").substring(0,42);
		}
		catch(Exception ex) {
			showStatus(ex.getMessage());
		}
		return "";
	}
	public void Send(int col) {
        byte[] bData = new byte[1];
        bData[0] = (byte) col;
		try {
	        DataOutputStream out = new DataOutputStream(skt.getOutputStream());
	        out.write(bData, 0, 1);
	        showStatus("Answer sent.");
		}
		catch(Exception ex) {
			showStatus(ex.getMessage());
		}
	}
}
