
public class ConnectorTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connector wc = new Connector("127.0.0.1",8001);
		Connector bc = new Connector("127.0.0.1",8002);
		while(true) { 
			String st = wc.Receive(); // 
			if(st.toLowerCase().indexOf("end game")== 0) {
				break;
			}
			System.out.println("White Move: Start state is "+st);
			// Add your method here and send the answer to the server 
			wc.Send(1); 
			
			st = bc.Receive(); // 
			if(st.toLowerCase().indexOf("end game")== 0) {
				break;
			}
			System.out.println("Black Move: Start state is "+st);
			bc.Send(2);
			
		}
		System.out.println("Game ended");
	}

}
