public interface PlayingStrategy {
	public void read(Board board);
	public int play();
}
