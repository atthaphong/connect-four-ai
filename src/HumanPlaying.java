import java.util.*;
public class HumanPlaying implements PlayingStrategy {
	Board board;
	Scanner kb;
	String name;
	String color;
	
	public HumanPlaying(int color){
		this.kb = new Scanner(System.in);
		System.out.println("What is your name?:");
		this.name = kb.nextLine();
		
		if(color==Board.WHITE){
			this.color = "White";
		}else{
			this.color = "Black";
		}
	}
	@Override
	public void read(Board board) {
		this.board = board;
		board.show();
	}

	@Override
	public int play() {
		System.out.println(name+",You're playing as "+color);
		System.out.print("Enter a column to fill in.");
		System.out.println("(1,"+board.cols+").");
		int col = kb.nextInt();
		return col;
	}

}
